"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deletePlayerById = exports.updatePlayerById = exports.postPlayer = exports.getPlayerById = exports.getPlayers = void 0;
const db_1 = require("../db");
const nanoid_1 = require("nanoid");
const getPlayers = (req, res) => {
    const data = (0, db_1.getConnection)()
        .get('players')
        .value();
    res.status(200).json(data);
};
exports.getPlayers = getPlayers;
const getPlayerById = (req, res) => {
    try {
        const data = (0, db_1.getConnection)().
            get('players').find({
            id: req.params.id
        })
            .value();
        if (data)
            res.status(200).json(data);
        else
            res.status(404).send('No player with that ID');
    }
    catch (error) {
        res.status(500).send(error);
    }
};
exports.getPlayerById = getPlayerById;
const postPlayer = (req, res) => {
    try {
        const { name, position, avg_points, avg_asist, avg_rebounds, avg_steals } = req.body;
        if (name && position && avg_points && avg_asist && avg_rebounds && avg_steals) {
            const player = { id: (0, nanoid_1.nanoid)(), name, position, avg_points, avg_asist, avg_rebounds, avg_steals };
            (0, db_1.getConnection)().get('players')
                .push(player).write();
            res.status(200).json(player);
        }
        else {
            res.status(400).json("Some fields no added");
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
};
exports.postPlayer = postPlayer;
const updatePlayerById = (req, res) => {
    try {
        const data = (0, db_1.getConnection)().
            get('players').find({
            id: req.params.id
        })
            .value();
        if (!data)
            res.status(404).send('No player with that ID');
        ;
        const updatePlayer = (0, db_1.getConnection)().
            get('players')
            .find({
            id: req.params.id
        })
            .assign(req.body)
            .write();
        res.status(200).json(updatePlayer);
    }
    catch (error) {
        res.status(500).send(error);
    }
};
exports.updatePlayerById = updatePlayerById;
const deletePlayerById = (req, res) => {
    try {
        const data = (0, db_1.getConnection)().
            get('players').find({
            id: req.params.id
        })
            .value();
        if (!data)
            res.status(404).send('No player with that ID');
        ;
        const deletePlayer = (0, db_1.getConnection)()
            .get('players')
            .remove({
            id: req.params.id
        })
            .write();
        res.json(deletePlayer);
    }
    catch (error) {
        res.status(500).send(error);
    }
};
exports.deletePlayerById = deletePlayerById;
