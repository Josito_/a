"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const players_controller_1 = require("../controller/players_controller");
const router = (0, express_1.Router)();
/**
 *  @swagger
 *  /players:
 *  get:
 *  summary: Return a list with all players
 *  responses:
 *      200:
 *      description: list with all players
 */
router.get('/players', players_controller_1.getPlayers);
router.get('/players/:id', players_controller_1.getPlayerById);
router.put('/players/:id', players_controller_1.updatePlayerById);
router.post('/players', players_controller_1.postPlayer);
router.delete('/players/:id', players_controller_1.deletePlayerById);
exports.default = router;
