"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.swaggerOptions = void 0;
exports.swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: 'Simple testing API Typescript',
            version: '1.0'
        },
        servers: [
            {
                url: 'http://localhost:3000'
            }
        ]
    },
    apis: [
        './routes/*.ts'
    ]
};
