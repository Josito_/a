import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import swaggerUI from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc'; 
import player_router from './routes/players.routes';
import { swaggerOptions } from './swaggerOptions';

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use('/swagger', swaggerUI.serve, swaggerUI.setup( swaggerJSDoc(swaggerOptions), { explorer: true } ))
app.use(player_router);

export default app;