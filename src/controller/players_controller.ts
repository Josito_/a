import { Handler, Request, Response } from "express";
import { getConnection } from "../db";
import { nanoid  } from "nanoid";
import { Players } from "../types/custom_types";

export const getPlayers: Handler = (req, res) => {
    const data = getConnection()
        .get('players')
        .value();
    res.status(200).json(data);
};

export const getPlayerById: Handler = (req: Request, res: Response) => {
    try {
        const data = getConnection().
            get('players').find({
                id: req.params.id
            })
            .value();
        if(data) res.status(200).json(data);
        else res.status(404).send('No player with that ID');
    } catch (error) {
        res.status(500).send(error);
    }
}

export const postPlayer: Handler = (req: Request, res: Response) => {
    try {
        const { name, position, avg_points, avg_asist, avg_rebounds, avg_steals } = req.body;
        if( name && position && avg_points && avg_asist && avg_rebounds && avg_steals) {
            const player: Players = { id: nanoid(), name, position, avg_points, avg_asist, avg_rebounds, avg_steals }
            getConnection().get('players')
            .push(player).write();
            res.status(200).json(player);
        } else {
            res.status(400).json("Some fields no added");
        }
    } catch (error) {
        res.status(500).send(error);
    }
}

export const updatePlayerById : Handler = (req: Request, res: Response) => {
    try {
        const data = getConnection().
        get('players').find({
            id: req.params.id
        })
        .value();
        
        if(!data) res.status(404).send('No player with that ID');;
        
        const updatePlayer = getConnection().
            get('players')
            .find({
                id: req.params.id
            })
            .assign(req.body)
            .write();
        
        res.status(200).json(updatePlayer);
    } catch (error) {
        res.status(500).send(error);
    }
}

export const deletePlayerById: Handler = (req: Request, res: Response) => {
    try {
        const data = getConnection().
        get('players').find({
            id: req.params.id
        })
        .value();
        
        if(!data) res.status(404).send('No player with that ID');;
        
        const deletePlayer = getConnection()
            .get('players')
            .remove({
                id: req.params.id
            })
            .write();
            res.status(200).send('Player deleted')
    } catch (error) {
        res.status(500).send(error);
    }
}