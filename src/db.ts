import lowdb from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import { Schema } from './types/custom_types';

let db: lowdb.LowdbSync<Schema>;

export const connection = async() => {
    const adapter = new FileSync<Schema>('data.json');
    db = lowdb(adapter);
    db.defaults({
        players: []
    }).write();
};


export const getConnection = () => db;