import app from './app';
import {connection} from './db';

connection();

app.listen(app.get('port'), () => {
    console.log("Listening")
})