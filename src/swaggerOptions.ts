export const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: 'Simple API Typescript',
            version: '1.0'
        },
        servers: [
            {
                url: 'http://localhost:3000'
            }
        ]
    },
    apis: [
        "./src/routes/*.ts"
    ]
}