export type Schema = {
    players: Players[]
}

export type Players = {
    id: string,
    name: string,
    position: Position,
    avg_points: number,
    avg_asist: number,
    avg_rebounds: number,
    avg_steals: number,
}


export type Position = 'point guard' | 'shooting guard' | 'power forward' | 'small forward' | 'center';
